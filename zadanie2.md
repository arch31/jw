artidasik@vm-juneway:~$ sudo chroot robota
bash-5.0# ls
ls: error while loading shared libraries: libselinux.so.1: cannot open shared object file: No such file or directory
bash-5.0# exit
exit
artidasik@vm-juneway:~$ cp /bin/ls robota/bin/
artidasik@vm-juneway:~$ ldd robota/bin/ls
        linux-vdso.so.1 (0x00007ffff3112000)
        libselinux.so.1 => /lib/x86_64-linux-gnu/libselinux.so.1 (0x00007f7416960000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f741679f000)
        libpcre.so.3 => /lib/x86_64-linux-gnu/libpcre.so.3 (0x00007f741672b000)
        libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f7416726000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f7416bb2000)
        libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f7416705000)
artidasik@vm-juneway:~$ cp /lib/x86_64-linux-gnu/libselinux.so.1 robota/lib
artidasik@vm-juneway:~$ cp /lib/x86_64-linux-gnu/libc.so.6 robota/lib
artidasik@vm-juneway:~$ cp /lib/x86_64-linux-gnu/libpcre.so.3 robota/lib
artidasik@vm-juneway:~$ cp /lib/x86_64-linux-gnu/libdl.so.2 robota/lib
artidasik@vm-juneway:~$ cp /lib64/ld-linux-x86-64.so.2 robota/lib64
artidasik@vm-juneway:~$ cp /lib/x86_64-linux-gnu/libpthread.so.0 robota/lib
artidasik@vm-juneway:~$ sudo chroot robota
bash-5.0# ls
bin  lib  lib64
bash-5.0# exit
exit
cp /bin/cp robota/bin/
artidasik@vm-juneway:~$ ls robota/bin/
bash  cp  ls
artidasik@vm-juneway:~$ ldd robota/bin/cp
        linux-vdso.so.1 (0x00007ffc6d392000)
        libselinux.so.1 => /lib/x86_64-linux-gnu/libselinux.so.1 (0x00007f82e239a000)
        libacl.so.1 => /lib/x86_64-linux-gnu/libacl.so.1 (0x00007f82e238f000)
        libattr.so.1 => /lib/x86_64-linux-gnu/libattr.so.1 (0x00007f82e2387000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f82e21c6000)
        libpcre.so.3 => /lib/x86_64-linux-gnu/libpcre.so.3 (0x00007f82e2152000)
        libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f82e214d000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f82e25ed000)
        libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f82e212a000)
artidasik@vm-juneway:~$ cp /lib/x86_64-linux-gnu/libselinux.so.1 robota/lib
artidasik@vm-juneway:~$ cp /lib/x86_64-linux-gnu/libacl.so.1 robota/lib
artidasik@vm-juneway:~$ cp /lib/x86_64-linux-gnu/libattr.so.1 robota/lib
artidasik@vm-juneway:~$ cp /lib/x86_64-linux-gnu/libc.so.6 robota/lib
artidasik@vm-juneway:~$ cp /lib/x86_64-linux-gnu/libpcre.so.3 robota/lib
artidasik@vm-juneway:~$ cp /lib/x86_64-linux-gnu/libdl.so.2 robota/lib
artidasik@vm-juneway:~$ cp /lib64/ld-linux-x86-64.so.2 robota/lib64
artidasik@vm-juneway:~$ cp /lib/x86_64-linux-gnu/libpthread.so.0 robota/lib
artidasik@vm-juneway:~$ sudo chroot robota
bash-5.0# ls
bin  lib  lib64
bash-5.0# ls bin/
bash  cp  ls
bash-5.0# cp bin/ls /
bash-5.0# ls
bin  lib  lib64  ls